# P7 - Les petits plats

## Découvrir le projet

Mon travail a été déployé sur une GitHub Pages à l'addresse suivante : 

## Lancement en local

Utilisez l'extension LiveServer sur VSCode puis lancez le serveur grâce au bouton "Go Live"

## Resultats de performances via JSBEN

Les résultats sont consultables via ce lien : https://jsben.ch/JroX5

## Objectif

Implémentation d'une fonctionnalité de recherche

## Attendus

Tester deux versions différentes pour l'algorithme de recherche de la barre principale :

 - Une version utilisant des boucles natives (while, for...)
 - Une version en programmation fonctionnelle avec les méthodes de l'objet array (foreach, filter, map, reduce)

Chacune des versions aura une branche Git afin de conserver les deux versions et veillez à bien séparer le code. La recherche par tags (ingrédients, ustensiles, appareils) sera la même pour les deux implémentations.

Une fiche d'investigation doit être remplie avec les deux implémentations différentes afin de les comparer. Cette fiche doit aussi comprendre un schéma afin de comprendre l'enchaînement des étapes.

Une fois les deux versions réalisées, il faudra choisir la meilleure en terme de performance (en utilisant l'outil jsben.ch). Puis ajouter les résultats à la fiche d’investigation de fonctionnalité rédigée. Enfin, terminer le document par la recommandation d’algorithme à garder.