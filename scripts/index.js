import recipeFactory from './factories/recipe.js'
import { recipes } from '../data/recipes.js'
import dropdownManager, { setElementsOnDropdown } from './utils/dropdown.js'
import filterManager from './utils/filter.js'

const recipesContainer = document.querySelector('.recipes')

for (const recipe of recipes) {
  const recipeCard = recipeFactory(recipe).getCard()
  recipesContainer.appendChild(recipeCard)
}

dropdownManager()
setElementsOnDropdown(recipes)
const searchBar = document.querySelector('.search-bar')
searchBar.addEventListener('input', () => filterManager())
