import filter from './filter.js'
import { normalizeString } from '../helpers/helpers.js'

export default function () {
    const dropdownContainers = document.querySelectorAll('.dropdown-container')
    for (const dropdownContainer of dropdownContainers) {
        dropdownContainer.addEventListener('click', () => openDropdown(dropdownContainer))
    }

    const searchBarsTags = document.querySelectorAll('.tags-search')
    for (const searchBarTags of searchBarsTags) {
        searchBarTags.addEventListener('focus', (e) => {
            const alwaysOneIsOpen = document.querySelector('#dropdown-open')
            if (alwaysOneIsOpen) {
                closeDropdown(e)
            }
            const dropdownContainer = searchBarTags.closest('.dropdown-container')
            openDropdown(dropdownContainer)
        })
        searchBarTags.addEventListener('input', (e) => {
            searchOnDropdown(searchBarTags, e)
        })
    }

    document.addEventListener('mouseup', (e) => closeDropdown(e))
}

function openDropdown(element) {
    if (element.id === 'dropdown-open') {
        return
    }
    const ul = element.querySelector('ul')
    ul.classList.remove('hidden')
    ul.setAttribute('aria-hidden', 'false')
    const img = element.querySelector('img')
    img.src = './assets/icons/up.svg'
    const input = element.querySelector('input')
    input.setAttribute('aria-expanded', 'true')
    element.setAttribute('id', 'dropdown-open')
    element.querySelector('input').focus()
}

function searchOnDropdown(input, event) {
    const { parentNode } = input
    const searchBarValue = normalizeString(event.target.value)
    const listContainer = parentNode.querySelectorAll("li")
    for (const element of listContainer) {
        normalizeString(element.textContent).includes(searchBarValue) ? element.style.display = 'list-item' : element.style.display = 'none'
    }
}

function closeDropdown(e) {
    const openedDropdown = document.querySelector('#dropdown-open')
    if (openedDropdown && !openedDropdown.contains(e.target)) {
        const ul = openedDropdown.querySelector('ul')
        openedDropdown.removeAttribute('id')
        const img = openedDropdown.querySelector('img')
        img.src = './assets/icons/down.svg'
        ul.classList.add('hidden')
        ul.setAttribute('aria-hidden', 'true')
        const input = openedDropdown.querySelector('input')
        input.setAttribute('aria-expanded', 'false')
        input.value = ''
    }
}

function deleteTag(e) {
    const { parentNode } = e.target
    parentNode.remove()
    filter()
}

function addTag(e) {
    const ul = document.querySelector('.tag-selection')
    const li = document.createElement('li')
    li.classList.add('tag-selected')
    const icon = document.createElement('img')
    icon.src = './assets/icons/delete.svg'
    icon.setAttribute('tabindex', '0')
    icon.classList.add('tag-selected-delete')
    icon.addEventListener('click', (e) => deleteTag(e))
    icon.addEventListener('keypress', (e) => {
        if (e.key === 'Enter') {
            deleteTag(e)
        }
    })
    const { parentNode } = e.target
    li.setAttribute("data-filter", parentNode.dataset.filter)
    li.append(e.target.textContent, icon)
    ul.appendChild(li)
    filter()
}

function setElementsOnDropdown(recipesList) {
    const dropdowns = document.querySelectorAll('ul[data-filter]')
    for (const dropdown of dropdowns) {
        dropdown.innerHTML = ''
        const { dataset } = dropdown
        const { filter } = dataset
        const listElements = []
        switch (filter) {
            case 'ingredients':
                recipesList.forEach(recipe => {
                    recipe.ingredients.forEach(ingredient => {
                        listElements.push(ingredient.ingredient.toLowerCase())
                    })
                })
                break
            case 'appareils':
                recipesList.forEach(recipe => {
                    listElements.push(recipe.appliance.toLowerCase())
                })
                break
            case 'ustensiles':
                recipesList.forEach(recipe => {
                    recipe.ustensils.forEach(ustensil => {
                        listElements.push(ustensil.toLowerCase())
                    })
                })
                break
            default:
                console.error("La liste d'éléments n'est pas connu")
        }

        const listWithoutDuplicates = [...new Set(listElements)]

        const tags = document.querySelectorAll('.tag-selected')
        for (const tag of tags) {
            for (const element of listWithoutDuplicates) {
                if (element === tag.textContent.toLowerCase()) {
                    listWithoutDuplicates.splice(listWithoutDuplicates.indexOf(element), 1)
                }
            }
        }

        for (const element of listWithoutDuplicates) {
            const li = document.createElement('li')
            li.textContent = element
            li.role = 'option'
            dropdown.append(li)
            li.addEventListener('click', (e) => addTag(e))
            li.addEventListener('keypress', (e) => {
                if (e.key === 'Enter') {
                    addTag(e)
                }
            })
        }
    }
}


export { addTag, setElementsOnDropdown, searchOnDropdown }