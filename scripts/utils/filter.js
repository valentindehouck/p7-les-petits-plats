import { recipes } from '/data/recipes.js'
import recipeFactory from '../factories/recipe.js'
import { setElementsOnDropdown } from './dropdown.js'
import { normalizeString } from '../helpers/helpers.js'

export default function () {
    const searchBarValue = document.querySelector('.search-bar').value
    const tags = document.querySelectorAll('.tag-selected')
    const filteredRecipes = filter(normalizeString(searchBarValue), tags)

    const recipesContainer = document.querySelector('.recipes')
    const emptyMessage = document.querySelector('span.emptyList')
    recipesContainer.innerHTML = ''
    filteredRecipes.length === 0 ? emptyMessage.classList.remove('hidden') : emptyMessage.classList.add('hidden')

    setElementsOnDropdown(filteredRecipes)
    for (const recipe of filteredRecipes) {
        const recipeCard = recipeFactory(recipe).getCard()
        recipesContainer.appendChild(recipeCard)
    }
}

function filter(searchBarValue, tags) {
    return recipes.filter(({ name, ingredients, description, appliance, ustensils }) => {
        return filterBySearchBar(searchBarValue, normalizeString(name), ingredients, normalizeString(description)) && filterByTags(tags, ingredients, appliance, ustensils)
    })
}

function filterBySearchBar(searchBarValue, name, ingredients, description) {
    return searchBarValue.length < 3 ||
        name.includes(searchBarValue) ||
        ingredients.some((ingredient) => normalizeString(ingredient.ingredient).includes(searchBarValue)) ||
        description.includes(searchBarValue)
}

function filterByTags(tags, ingredients, appliance, ustensils) {
    return Array.from(tags).every(({ dataset, textContent }) => {
        const { filter } = dataset
        const tagName = textContent.toLowerCase()

        switch (filter) {
            case "ingredients":
                return ingredients.some(
                    (ingredient) => ingredient.ingredient.toLowerCase() === tagName
                )
            case "appareils":
                return appliance.toLowerCase().includes(tagName)
            case "ustensiles":
                return ustensils.some((ustensil) => ustensil.toLowerCase() === tagName)
            default:
                return false
        }
    })
}