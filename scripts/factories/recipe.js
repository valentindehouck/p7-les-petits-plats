export default function (recipe) {
    const { id, name, servings, ingredients, time, description, appliance, ustensils } = recipe

    function getCard() {
        const article = document.createElement('article')
        article.classList.add('card')

        const cover = document.createElement('div')
        cover.classList.add('card-img')

        const content = document.createElement('div')
        content.classList.add('card-content')

        const header = document.createElement('div')
        header.classList.add('card-header')

        const infos = document.createElement('div')
        infos.classList.add('card-details')

        const title = document.createElement('h3')
        title.textContent = name
        const timing = document.createElement('p')
        timing.textContent = time + ' min'
        header.append(title, timing)

        const listIngredients = document.createElement('ul')
        listIngredients.classList.add('card-ingredients')
        for (const list of ingredients) {
            const { ingredient, quantity, unit } = list
            const elt = document.createElement('li')
            elt.innerHTML = `<strong>${ingredient + (quantity ? ' : ' : '')}</strong> ${(quantity ? quantity : '') + (unit ? (' ' + unit) : '')}`
            listIngredients.append(elt)
        }

        const desc = document.createElement('p')
        desc.textContent = description
        infos.append(listIngredients, desc)
        content.append(header, infos)
        article.append(cover, content)
        return article
    }

    return { getCard }
}